<?php $this->load->view("templates/header"); ?>

    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
      <h5 class="my-0 mr-md-auto font-weight-normal">Web Framework 101</h5>
      <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="#"></a>
        <a class="p-2 text-dark" href="#">Home</a>
        <a class="p-2 text-dark" href="#">About</a>
        <a class="p-2 text-dark" href="#">Blog</a>
      </nav>
      <a class="btn btn-outline-primary" href="#">Login</a>
    </div>

    <div class="container">
      
      <!-- Begin page content -->
      <main role="main" class="container">
        <h1 class="mt-5"><?php echo $judul ?></h1>
		
		<h1>Manual Array</h1>
        <ul>
		<?php 
			foreach ($biodata_manual_array as $key) { ?>
			
        	<li><?php echo $key['nim'] ?> - <?php echo $key['alamat'] ?></li>
			
		<?php } ?>
		</ul>		
		<h1>Manual Object</h1>
        <ul>
		<?php 
			foreach ($biodata_manual_object as $key) { ?>
			
        	<li><?php echo $key->nim ?> - <?php echo $key->alamat ?></li>
			
		<?php } ?>
		</ul>		
		<h1>Builder Array</h1>
        <ul>
		<?php 
			foreach ($biodata_builder_array as $key) { ?>
			
        	<li><?php echo $key['nim'] ?> - <?php echo $key['alamat'] ?></li>
			
		<?php } ?>
		</ul>		
		<h1>Builder Object</h1>
        <ul>
		<?php 
			foreach ($biodata_builder_object as $key) { ?>
			
        	<li><?php echo $key->nim ?> - <?php echo $key->alamat ?></li>
			
		<?php } ?>
		</ul>
        
      </main>

      <footer class="pt-4 my-md-5 pt-md-5 border-top">
        <div class="row">
          <div class="col-12 col-md">
            <img class="mb-2" src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" alt="" width="24" height="24">
            <small class="d-block mb-3 text-muted">&copy; 2017-2018</small>
          </div>
          <div class="col-6 col-md">
            <h5>Features</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Cool stuff</a></li>
              <li><a class="text-muted" href="#">Random feature</a></li>
              <li><a class="text-muted" href="#">Team feature</a></li>
              <li><a class="text-muted" href="#">Stuff for developers</a></li>
              <li><a class="text-muted" href="#">Another one</a></li>
              <li><a class="text-muted" href="#">Last time</a></li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>Resources</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Resource</a></li>
              <li><a class="text-muted" href="#">Resource name</a></li>
              <li><a class="text-muted" href="#">Another resource</a></li>
              <li><a class="text-muted" href="#">Final resource</a></li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>About</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Team</a></li>
              <li><a class="text-muted" href="#">Locations</a></li>
              <li><a class="text-muted" href="#">Privacy</a></li>
              <li><a class="text-muted" href="#">Terms</a></li>
            </ul>
          </div>
        </div>
      </footer>
    </div>

<?php $this->load->view("templates/footer"); ?>