<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{

		$this->load->model( 'biodata' );

		$data['judul'] = "List Mahasiswa";

		$data['biodata_manual_array'] = $this->biodata->get_biodata_manual_array();
		$data['biodata_manual_object'] = $this->biodata->get_biodata_manual_object();
		$data['biodata_builder_array'] = $this->biodata->get_biodata_builder_array();
		$data['biodata_builder_object'] = $this->biodata->get_biodata_builder_object();


		$this->load->view( 'home', $data );

	}

	public function view($id) {

		$this->load->model( 'biodata' );

		$data['judul'] = "Detail #" .$id;

		$data['data_biodata'] = $this->biodata->get_single_biodata($id);

		$this->load->view( 'home_detail', $data );

	}

	public function create() {

	}

	public function edit() {

	}

	public function delete() {

	}

}
