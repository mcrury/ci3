<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biodata extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		
	}

	// GET BIODATA SECARA QUERY MANUAL / OBJECT
	public function get_biodata_manual_object() {
		$query = $this->db->query("
			SELECT * FROM biodata
		");
		return $query->result();
	}

	// GET BIODATA SECARA QUERY MANUAL / ARRAY
	public function get_biodata_manual_array() {
		$query = $this->db->query("
			SELECT * FROM biodata
		");
		return $query->result_array();
	}

	// GET BIODATA SECARA QUERY BUILDER / OBJECT
	public function get_biodata_builder_object() {
		$query = $this->db->get("biodata");
		return $query->result();
	}

	// GET BIODATA SECARA QUERY BUILDER / OBJECT
	public function get_biodata_builder_array() {
		$query = $this->db->get("biodata");
		return $query->result_array();
	}

	public function get_single_biodata ($id) {
		$query = $this->db->get_where( 'biodata', array('id' => $id));
		return $query->result();
	}


}
