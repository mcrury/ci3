(function($){
	$(document).ready(function(){
	})

	function setEqualHeight(selector) {
	    if (selector.length > 0) {
	        var arr = [];
	        var selector_height;
	        selector.css("min-height", "initial");
	        selector.each(function (index, elem) {
	            selector_height = elem.offsetHeight;
	            arr.push(selector_height);
	        });
	        selector_height = Math.max.apply(null, arr);
	        selector.css("min-height", selector_height);
	    }
	}
	setEqualHeight(jQuery(".wdm-acc-content-title"));
})(jQuery);